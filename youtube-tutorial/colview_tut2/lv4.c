#include <gtk/gtk.h>

#define WORDS_FILE "/usr/share/dict/words"

static GtkWidget *search_entry;

static GListModel *
words_list_model_creator (gpointer item, gpointer user_data)
{
	GtkStringObject *strobj = GTK_STRING_OBJECT(item);
	g_autoptr(GListStore) store = g_list_store_new (GTK_TYPE_STRING_OBJECT);

	if (! strobj)
	{
		for (int i = 'a'; i <= 'z'; ++i)
		{
			g_autoptr(GtkStringObject) tmp = NULL;
			char buf[2] = {0};

			sprintf (buf, "%c", i);
			tmp = gtk_string_object_new (buf);
			g_list_store_append (store, tmp);
		}

		return G_LIST_MODEL(g_steal_pointer (&store));
	}
	else if (strlen (gtk_string_object_get_string (strobj)) == 1)
	{
		static char *contents;
		static GStrv lines;

		GtkExpression *expr = gtk_property_expression_new (GTK_TYPE_STRING_OBJECT, NULL, "string");
		GtkStringFilter *string_filter = gtk_string_filter_new (expr);
		GtkFilterListModel *filter_model;

		if (! contents)
		{
			g_autoptr(GError) error = NULL;

			g_file_get_contents (WORDS_FILE, &contents, NULL, &error);

			if (error)
				g_error ("%s", error->message);

			lines = g_strsplit (contents, "\n", -1);
		}

		/* Equivalent to what we did with the 'search-mode-enabled' property in the .ui file.
		 * Bind the "text" property of the GtkSearchEntry (actually a property
		 * of the GtkEditable interface which it implements) to the "search" property of the
		 * filter.
		 *
		 * G_BINDING_SYNC_CREATE means synchronize the source and target
		 * properties upon creation (direction of the binding remains from the
		 * source to the target.
		 *
		 * IME, unlike bidirectional bindings, which are more obvious when they
		 * are needed -- it can be hard to decide whether to use
		 * G_BINDING_DEFAULT or G_BINDING_SYNC_CREATE - sometimes I find you
		 * need to try 'default' first (as it likely uses less overhead) and
		 * then use 'sync-create' if it's not working as expected.
		 */
		g_object_bind_property (search_entry, "text", string_filter, "search", G_BINDING_SYNC_CREATE);

		/* The equivalent can be done with expressions/gtk_expression_bind like so:
		 *   (Personally, I think using g_object_bind_property is more straightforward) */
#if 0
		{
			GtkExpression *prop_expr = gtk_property_expression_new (GTK_TYPE_EDITABLE, NULL, "text");
			gtk_expression_bind (prop_expr, string_filter, "search", search_entry);
		}
#endif

		for (int i = 0; lines[i]; ++i)
		{
			int ltr = gtk_string_object_get_string (strobj)[0];

			if (lines[i][0] == ltr)
			{
				g_autoptr(GtkStringObject) tmp = gtk_string_object_new (lines[i]);
				g_list_store_append (store, tmp);
			}
		}

		filter_model = gtk_filter_list_model_new (G_LIST_MODEL(g_steal_pointer (&store)), GTK_FILTER(string_filter));

		return G_LIST_MODEL(filter_model);
	}
	else
	{
		return NULL;
	}

}

static void
setup_listview (GtkListView *list_view)
{
	GListModel *model = words_list_model_creator (NULL, NULL);
	GtkTreeListModel *treemodel = gtk_tree_list_model_new (model,
		/* gboolean passthrough, */				FALSE,
		/* gboolean autoexpand, */				FALSE,
		/* GtkTreeListModelCreateModelFunc */	words_list_model_creator,
		/* gpointer user_data, */				NULL,
		/* GDestroyNotify user_destroy */		NULL);
	GtkSelectionModel *selection = GTK_SELECTION_MODEL(gtk_single_selection_new (G_LIST_MODEL(treemodel)));

	gtk_list_view_set_model (list_view, selection);
}

static void
activate (GtkApplication *app, gpointer user_data)
{
	g_autoptr(GtkBuilder) builder = gtk_builder_new_from_file ("lv4.ui");
	GtkWidget *window = (gpointer) gtk_builder_get_object (builder, "window");
	GtkListView *list_view = (gpointer) gtk_builder_get_object (builder, "list_view");

	search_entry = (gpointer) gtk_builder_get_object (builder, "search_entry");
		
	setup_listview (list_view);

	gtk_application_add_window (app, GTK_WINDOW(window));
	gtk_window_present (GTK_WINDOW(window));
}

int
main (int argc, char *argv[])
{
	g_autoptr(GtkApplication) app = NULL;
	int status;

	G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	G_GNUC_END_IGNORE_DEPRECATIONS
	g_signal_connect (app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run (G_APPLICATION(app), argc, argv);

	return status;
}
