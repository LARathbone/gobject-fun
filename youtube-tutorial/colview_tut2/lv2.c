#include <gtk/gtk.h>

static void
add_stuff_to_store (GListStore *store)
{
	g_autoptr(GtkStringObject) strobj1 = gtk_string_object_new ("foo");
	g_autoptr(GtkStringObject) strobj2 = gtk_string_object_new ("bar");

	g_list_store_append (store, strobj1);
	g_list_store_append (store, strobj2);
}

static void
add_stuff_to_tree_store (GListStore *store)
{
	g_autoptr(GtkStringObject) strobj1 = gtk_string_object_new ("sub-foo");
	g_autoptr(GtkStringObject) strobj2 = gtk_string_object_new ("sub-bar");

	g_list_store_append (store, strobj1);
	g_list_store_append (store, strobj2);
}

static GListModel *
tree_list_model_creator (gpointer item, gpointer user_data)
{
	GtkStringObject *strobj = GTK_STRING_OBJECT(item);
	const char *str = gtk_string_object_get_string (strobj);
	GListStore *new_store;
	
	if (strncmp (str, "sub-", strlen("sub-")) == 0)
		return NULL;

	new_store = g_list_store_new (GTK_TYPE_STRING_OBJECT);
	add_stuff_to_tree_store (new_store);

	return G_LIST_MODEL(new_store);
}

static void
setup_listview (GtkListView *list_view)
{
	GListStore *store = g_list_store_new (GTK_TYPE_STRING_OBJECT);
	GtkTreeListModel *treemodel = gtk_tree_list_model_new (G_LIST_MODEL(store),
		/* gboolean passthrough, */				FALSE,
		/* gboolean autoexpand, */				FALSE,
		/* GtkTreeListModelCreateModelFunc */	tree_list_model_creator,
		/* gpointer user_data, */				NULL,
		/* GDestroyNotify user_destroy */		NULL);
	GtkSelectionModel *selection = GTK_SELECTION_MODEL(gtk_single_selection_new (G_LIST_MODEL(treemodel)));

	add_stuff_to_store (store);
	gtk_list_view_set_model (list_view, selection);
}

static void
activate (GtkApplication *app, gpointer user_data)
{
	g_autoptr(GtkBuilder) builder = gtk_builder_new_from_file ("lv2.ui");
	GtkWidget *window = (gpointer) gtk_builder_get_object (builder, "window");
	GtkListView *list_view = (gpointer) gtk_builder_get_object (builder, "list_view");
		
	setup_listview (list_view);

	gtk_application_add_window (app, GTK_WINDOW(window));
	gtk_window_present (GTK_WINDOW(window));
}

int
main (int argc, char *argv[])
{
	g_autoptr(GtkApplication) app = NULL;
	int status;

	G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	G_GNUC_END_IGNORE_DEPRECATIONS
	g_signal_connect (app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run (G_APPLICATION(app), argc, argv);

	return status;
}
