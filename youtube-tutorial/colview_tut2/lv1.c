#include <gtk/gtk.h>

static void
setup_cb (GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
	GtkWidget *expander = gtk_tree_expander_new ();
	GtkWidget *label = gtk_label_new ("");

	gtk_tree_expander_set_child (GTK_TREE_EXPANDER(expander), label);
	gtk_list_item_set_child (list_item, expander);
}

static void
bind_cb (GtkSignalListItemFactory *self, GtkListItem *list_item, gpointer user_data)
{
	GtkTreeListRow *row = gtk_list_item_get_item (list_item);
	GtkStringObject *strobj = gtk_tree_list_row_get_item (row);
	GtkWidget *expander = gtk_list_item_get_child (list_item);
	GtkWidget *label = gtk_tree_expander_get_child (GTK_TREE_EXPANDER(expander));

	gtk_tree_expander_set_list_row (GTK_TREE_EXPANDER(expander), row);
	gtk_label_set_text (GTK_LABEL(label), gtk_string_object_get_string (strobj));
}

static void
add_stuff_to_store (GListStore *store)
{
	g_autoptr(GtkStringObject) strobj1 = gtk_string_object_new ("foo");
	g_autoptr(GtkStringObject) strobj2 = gtk_string_object_new ("bar");

	g_list_store_append (store, strobj1);
	g_list_store_append (store, strobj2);
}

static void
add_stuff_to_tree_store (GListStore *store)
{
	g_autoptr(GtkStringObject) strobj1 = gtk_string_object_new ("sub-foo");
	g_autoptr(GtkStringObject) strobj2 = gtk_string_object_new ("sub-bar");

	g_list_store_append (store, strobj1);
	g_list_store_append (store, strobj2);
}

static GtkListItemFactory *
create_factory (void)
{
	GtkListItemFactory *factory = gtk_signal_list_item_factory_new ();

	g_signal_connect (factory, "setup", G_CALLBACK(setup_cb), NULL);
	g_signal_connect (factory, "bind", G_CALLBACK(bind_cb), NULL);

	return factory;
}

static GListModel *
tree_list_model_creator (gpointer item, gpointer user_data)
{
	GtkStringObject *strobj = GTK_STRING_OBJECT(item);
	const char *str = gtk_string_object_get_string (strobj);
	GListStore *new_store;
	
	/* Only create a new list store if the string contained within the item
	 * does NOT start with "sub-"
	 */
	if (strncmp (str, "sub-", strlen("sub-")) == 0)
		return NULL;

	new_store = g_list_store_new (GTK_TYPE_STRING_OBJECT);
	add_stuff_to_tree_store (new_store);

	return G_LIST_MODEL(new_store);
}

static GtkWidget *
create_list_view (void)
{
	GListStore *store = g_list_store_new (GTK_TYPE_STRING_OBJECT);

	/* The GtkTreeListModel constructor takes many parameters, several of which
	 * instantiate aspects of it which are NOT GObject properties. As such, it
	 * essentially needs to be instantiated in code.
	 */
	GtkTreeListModel *treemodel = gtk_tree_list_model_new (
		/* GListModel* root, */					G_LIST_MODEL(store),
		/* "If FALSE, the GListModel functions for this object return custom
		 * GtkTreeListRow objects. If TRUE, the values of the child models are
		 * pass through unmodified." --> setting to FALSE because we *want* the
		 * custom GtkTreeListRow objects to be created.
		 */
		/* gboolean passthrough, */				FALSE,
		/* gboolean autoexpand, */				FALSE,
		/* GtkTreeListModelCreateModelFunc */	tree_list_model_creator,
		/* gpointer user_data, */				NULL,
		/* GDestroyNotify user_destroy */		NULL);

	GtkSelectionModel *selection = GTK_SELECTION_MODEL(gtk_single_selection_new (G_LIST_MODEL(treemodel)));
	GtkListItemFactory *factory = create_factory ();
	GtkWidget *list_view = gtk_list_view_new (selection, factory);

	add_stuff_to_store (store);

	return list_view;
}

static void
activate (GtkApplication *app, gpointer user_data)
{
	GtkWidget *window = gtk_window_new ();
	GtkWidget *scrolled_window = gtk_scrolled_window_new ();
	GtkWidget *list_view = create_list_view ();

	gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW(scrolled_window), list_view);
	gtk_window_set_child (GTK_WINDOW(window), scrolled_window);
	gtk_window_set_default_size (GTK_WINDOW(window), 400, 400);
	gtk_application_add_window (app, GTK_WINDOW(window));

	gtk_window_present (GTK_WINDOW(window));
}

int
main (int argc, char *argv[])
{
	g_autoptr(GtkApplication) app = NULL;
	int status;

	G_GNUC_BEGIN_IGNORE_DEPRECATIONS
	app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
	G_GNUC_END_IGNORE_DEPRECATIONS
	g_signal_connect (app, "activate", G_CALLBACK(activate), NULL);
	status = g_application_run (G_APPLICATION(app), argc, argv);

	return status;
}
